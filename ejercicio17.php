<?php
    function vocales($vocales) {
        $vocales= strtolower($vocales);
        $contador=0;
        for($c=0;$c<strlen($vocales);$c++){
            if($vocales[$c]=="a" ||
               $vocales[$c]=="e" ||
               $vocales[$c]=="i" ||
               $vocales[$c]=="o" ||
               $vocales[$c]=="u"
                    ){
                $contador++;
            }
        }
        return $contador;
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $texto= "Ejercicio diecisiete de la practica 13";
            echo vocales($texto);
        ?>
    </body>
</html>
