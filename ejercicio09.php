<?php

function numeros($vector) {
    $negativos = false;
    $salida = "";    
    foreach ($vector as $value) {
        if($value<0){
            $negativos= true;
        }
        
        if($negativos==true){
            $salida = "Hay negativos<br>";
        }else{
            $salida = "Siempre Positivo nunca negativo<br>";
        }
    }
    return $salida;

    
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        echo numeros([1, 5, 6, -8, 78, 65]);
        
        echo numeros([1, 5, 6, 8, 78, 65]);
        ?>
    </body>
</html>
