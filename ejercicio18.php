<?php
//    function vocales($vocales) {
//        $v=["a","e","i","o","u"];
//        $imprimeVocales = [];
//        for ($c=0;$c<strlen($vocales);$c++){
//            if(in_array(strtolower($vocales[$c]),$v)){
//                $imprimeVocales[]=$vocales[$c];
//            }
//        }
//        return $imprimeVocales;
//    }
    
    
    function vocales($vocales){
        
        $solucion=array_filter(str_split($vocales),function($caracter){
            $v=["a","e","i","o","u"];
            if(in_array(strtolower($caracter),$v)){
                return true;
            }
        });
        
        return $solucion;
    }
    

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
           $texto = "Ejemplo de clase";
           
           var_dump(vocales($texto));
        ?>
    </body>
</html>
