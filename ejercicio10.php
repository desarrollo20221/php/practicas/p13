<?php
    function suma($a,$b,$c) {
        $s1= array_sum($a);
        $s2= array_sum($b);
        $s3= array_sum($c);
        
        $suma = $s1+$s2+$s3;
        return $suma;
        
        }

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $vector1 = [
                2,4,6
            ];
            $vector2 = [
                1,2,3
            ];
            $vector3 = [
                5,4,3
            ];
            echo suma($vector1, $vector2, $vector3);
        ?>
    </body>
</html>
